import { Injectable } from '@angular/core';

@Injectable()
export class PersonalStorageService {

   allPersonal = [
    {
       id : 1,
       name : 'Samuel',
       role : 'dispatcher1'
    },
    {
        id : 2,
        name : 'James',
        role : 'director'
    },
    {
        id : 3,
        name : 'Peter',
        role: 'admin'
    }
    ];

}
