import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import 'rxjs';
// import { SessionService } from './auth/session.service/session.service';
// import { IUser } from './user/user.interfaces';

@Injectable()
export class AuthService {

  public isAuthenticated = true;

  constructor(
    private http: HttpClient,
    private router: Router,
    // private session: SessionService
  ) {

  }
  /*
  public login(email: string, pass: string): Observable<any> | any {
    let req: Observable<any> = this.http.post<any>('v1/auth/login',
      { email, pass },
      { observe: 'response' }).share();
    req.subscribe((res: HttpResponse<any>) => {
      if (res.status === 200) {
        this.session.setSessionData(res.body.data);
        this.router.navigate(['/main/devices']);
      }
    }, (err) => {
      return Observable.throw(err);
    });

    return req;
  }

  public logout(): void {
    this.session.forgetUser();
    this.router.navigate(['/auth']);
  }

  public signUp(user: IUser): Observable<any> {
    return this.http.post('v1/auth/signup', { user });
  }

  public confirmEmail(key: string): Observable<any> {
    return this.http.post(`v1/auth/email-confirmation/${key}`, {});
  }

  public requestPasswordReset(email: string): Observable<any> {
    return this.http.post('v1/auth/reset-password', { email });
  }

  public resetPassword(resetToken: string, pass: string): Observable<any> {
    return this.http.post(`v1/auth/reset-password/new/${resetToken}`, { pass });
  }

   // public isAuthenticated: boolean = false;

   // public userRole: string = '';

   */
}
