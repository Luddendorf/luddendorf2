import { Injectable } from '@angular/core';
import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';

// import { SessionService } from './session.service/session.service';
import { AuthService } from './auth.service';

@Injectable()
export class AuthGuard implements CanActivate {



  constructor(public auth: AuthService, public router: Router) {}
  canActivate(): boolean {
    if (!this.auth.isAuthenticated) {
      this.router.navigate(['login']);
      return false;
    }
    return true;
  }
   /*
    constructor(
    private auth: AuthService,
    private router: Router,
    // private session: SessionService
  ) { }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    if (this.session.token) {
      return true;
    }
    this.router.navigate(['/auth/login']);
    return false;
  }                  }) */
}

