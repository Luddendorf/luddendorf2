export interface IPerson {
    id?: number;
    name?: string;
    role?: string;
}
