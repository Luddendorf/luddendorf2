import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatButtonModule } from '@angular/material';
import { MatListModule } from '@angular/material/list';
import {MatIconModule} from '@angular/material/icon';
import {MatDialogModule} from '@angular/material/dialog';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

import { PersonalService } from '../services/personal.service';
import { PersonEditFormComponent } from './person-edit-form/person-edit-form.component';
import { IPerson } from '../interfaces/IPerson';


@Component({
  selector: 'app-personal',
  templateUrl: './personal.component.html',
  styleUrls: ['./personal.component.css']
})
export class PersonalComponent implements OnInit, OnDestroy {

   personEditFormRef: MatDialogRef<PersonEditFormComponent>;

  constructor(
    // private personalService: PersonalService,
      private dialog: MatDialog
  ) {
  }

  allMyPersonal = [
    {
       id : 1,
       name : 'Samuel',
       role : 'dispatcher1'
    },
    {
        id : 2,
        name : 'James',
        role : 'director'
    },
    {
        id : 3,
        name : 'Peter',
        role: 'admin'
    }
    ];

  ngOnInit() {

  }

  ngOnDestroy() {
  }

  openPersonForm() {
    const personEditFormRef = this.dialog.open(PersonEditFormComponent, {
      width: '500px',
      height: '350px',
      /* position: {
        top: '200px',
        left: '200px',
      }, */
      disableClose: true,
      data: {
        id: 23,
        name: 'Ivan',
        role: 'dispatcher1'
      }
    });

    personEditFormRef.afterOpen()
      .subscribe(observer => {

      });

    personEditFormRef.afterClosed()
      .subscribe(result => {
        if (Number.isInteger(result)) {
            // this.personalService.deletePerson('personal', result.id);
        } else {
           //  this.personalService.updatePerson('personal', result);
        }
      });
  }

  createPersonForm() {
    const personEditFormRef = this.dialog.open(PersonEditFormComponent, {
      width: '500px',
      height: '350px',
      /* position: {
        top: '200px',
        left: '200px',
      }, */
      disableClose: true,
      data: {
      }
    });

    personEditFormRef.afterOpen()
      .subscribe(observer => {

      });

    personEditFormRef.afterClosed()
      .subscribe(result => {
        // this.personalService.updatePerson('personal', result);
      });
  }


}
  /*
  subscriptionForPersonal: Subscription;

  allPersonal = [];

  urlFragment = 'personal';

  public selectedPerson: any;

  public personForm: FormGroup;

  Person: IPerson;

  constructor(
    private personalService: PersonalService,
    //private formBuilder: FormBuilder
  ) { }

  ngOnInit() {

    this.subscriptionForPersonal = this.personalService.personal
            .subscribe(
              (data: any) => {
                this.allPersonal = data;
                            });
  }

  ngOnDestroy() {

    this.subscriptionForPersonal.unsubscribe();
  }

  public createNewPerson(urlFragment, person): void {

    this.personalService.createPerson(this.urlFragment, person);
  }


  public updateSelectedPerson(urlFragment, person) {

    this.personalService.updatePerson(this.urlFragment, person);
  }

  public deleteSelectedPerson(urlFragment, id) {

    this.personalService.deletePerson(this.urlFragment, id);
  }
                                  }

  public cancelPersonCreation (event): void {
     // this.modalRef.close(null);
  }

  public createPerson(): void {
    if (this.personForm.valid) {
    //  this.modalRef.close(this.personForm);
    } else {
    }
  }

  public deletePerson(): void {
    // this.modalRef.close(this.person.id);
  }
  */























/*
import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatButtonModule } from '@angular/material';
import { MatListModule } from '@angular/material/list';
import {MatIconModule} from '@angular/material/icon';
import {MatDialogModule} from '@angular/material/dialog';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

import { PersonalService } from '../services/personal.service';
import { PersonEditFormComponent } from './person-edit-form/person-edit-form.component';
import { IPerson } from '../interfaces/IPerson';
// import { TestFormComponent } from './test-form/test-form/test-form.component';

@Component({
  selector: 'app-personal',
  templateUrl: './personal.component.html',
  styleUrls: ['./personal.component.css']
})
export class PersonalComponent implements OnInit, OnDestroy {

   personEditFormRef: MatDialogRef<PersonEditFormComponent>;

  constructor(
    // private personalService: PersonalService,
      private dialog: MatDialog
  ) {
  }

  allMyPersonal = [
    {
       id : 1,
       name : 'Samuel',
       role : 'dispatcher1'
    },
    {
        id : 2,
        name : 'James',
        role : 'director'
    },
    {
        id : 3,
        name : 'Peter',
        role: 'admin'
    }
    ];

  ngOnInit() {

  }

  ngOnDestroy() {
  }

  openPersonForm() {
   /* const dialogConfig = new MatDialogConfig();

    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;

    dialogConfig.data = {
        id: 1,
        title: 'Angular For Beginners'
    };

    this.dialog.open(CourseDialogComponent, dialogConfig);
    const personEditFormRef = this.dialog.open(PersonEditFormComponent, {
      width: '500px',
      height: '800px',
      position: {
        top: '200px',
        left: '200px',
      },
      disableClose: true,
      data: {
        id: 23,
        name: 'Ivan',
        role: 'dispatcher1'
      }
    });

    personEditFormRef.afterOpen()
      .subscribe(observer => {

      });

    personEditFormRef.afterClosed()
      .subscribe(result => {
        if (Number.isInteger(result)) {
            // this.personalService.deletePerson('personal', result.id);
        } else {
           //  this.personalService.updatePerson('personal', result);
        }
      });
  }
}
  /*
  subscriptionForPersonal: Subscription;

  allPersonal = [];

  urlFragment = 'personal';

  public selectedPerson: any;

  public personForm: FormGroup;

  Person: IPerson;

  constructor(
    private personalService: PersonalService,
    //private formBuilder: FormBuilder
  ) { }

  ngOnInit() {

    this.subscriptionForPersonal = this.personalService.personal
            .subscribe(
              (data: any) => {
                this.allPersonal = data;
                            });
  }

  ngOnDestroy() {

    this.subscriptionForPersonal.unsubscribe();
  }

  public createNewPerson(urlFragment, person): void {

    this.personalService.createPerson(this.urlFragment, person);
  }


  public updateSelectedPerson(urlFragment, person) {

    this.personalService.updatePerson(this.urlFragment, person);
  }

  public deleteSelectedPerson(urlFragment, id) {

    this.personalService.deletePerson(this.urlFragment, id);
  }
                                  }

  public cancelPersonCreation (event): void {
     // this.modalRef.close(null);
  }

  public createPerson(): void {
    if (this.personForm.valid) {
    //  this.modalRef.close(this.personForm);
    } else {
    }
  }

  public deletePerson(): void {
    // this.modalRef.close(this.person.id);
  }
  */
