import { Component, OnInit, Inject, ChangeDetectionStrategy } from '@angular/core';
import { FormGroup, FormBuilder, Validators,
  FormControl, FormControlName, ReactiveFormsModule } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA, MatInputModule } from '@angular/material';
import { MatSelectModule } from '@angular/material/select';

import { PersonalService } from '../../services/personal.service';

@Component({
  selector: 'app-person-edit-form',
  templateUrl: './person-edit-form.component.html',
  styleUrls: ['./person-edit-form.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PersonEditFormComponent implements OnInit {

  newPersonMode = false;

  namePlaceholder = 'Ivan Ivanov';

  private person = 'Ivan Ivanov';

  public personEditForm: FormGroup;

  allPersonalFake = [
    {
       id : 1,
       name : 'Samuel',
       role : 'dispatcher1'
    },
    {
        id : 2,
        name : 'James',
        role : 'director'
    },
    {
        id : 3,
        name : 'Peter',
        role: 'admin'
    }
    ];

  rolesFake = [
    'Диспетчер (начинающий)',
    'Диспетчер (опытный)',
    'Старший диспетчер',
    'Логист',
    'Менеджер по персоналу',
    'Менеджер по персоналу+',
    'Бухгалтер',
    'Директор',
    'Администратор',
    'Картограф'
    /*
    'dispatcher1',
    'dispatcher2',
    'dispatcher3',
    'logistic',
    'HRmanager1',
    'HRmanager2',
    'accountant',
    'director',
    'admin',
    'map-manager' */
     ];

  constructor(
    private personalService: PersonalService,
    public modalRef: MatDialogRef<PersonEditFormComponent>,
    private formBuilder: FormBuilder,
    @Inject(MAT_DIALOG_DATA) public data: any  // ACHTUNG!
  ) {
     if (!data.id) { this.newPersonMode = true; }

    this.personEditForm = new FormGroup({
      id: new FormControl(data.id ? data.id : ''),
      name: new FormControl(data.name && this.newPersonMode ? data.name : '', Validators.required),
      role: new FormControl(data.role ? data.role : '', Validators.required)
    });

    this.personEditForm.setValue({
      id: this.allPersonalFake[0].id,
      name: this.allPersonalFake[0].name,
      role: this.allPersonalFake[0].role
    });

    /*
      this.personEditForm = this.formBuilder.group({
      id: [data.id ? data.id : ''],
      name: [data.name && this.newPersonMode ? data.name : '', Validators.required],
      role: [data.role ? data.role : '', Validators.required]
    /*name: [data.person && data.person.name ? data.person.name : '', Validators.required],
      role: [data.person && data.person.role ? data.person.role : '']

      this.recipeForm = new FormGroup({
      'name': new FormControl(recipeName, Validators.required),
      'imagePath': new FormControl(recipeImagePath, Validators.required),
    });
    this.userForm = new FormGroup({
      UserFirstName: new FormControl(),
      UserLastName: new FormControl()
   });


    */

  }

  ngOnInit() {
    //  we don't know if we are making a new person or editing old one.
    // this.newPersonMode = !this.data;
  }

  public createPerson(): void {
    this.modalRef.close(this.personEditForm);
  }

  public cancelPersonEdit(event): void {
    this.modalRef.close(null);
  }

  public editPerson(): void {
    if (this.personEditForm.valid) {
      this.modalRef.close(this.personEditForm);
    } else {
    //  FormUtils.markAllFormFieldsAsTouched(this.personEditForm);
    }
  }

  public deletePerson(): void {
    this.modalRef.close(this.data.person.id);
    let approve = false;
    approve = confirm('Вы точно хотите удалить сотрудника?');
      if (approve) {
       } else { return; }
  }
   /*
  public onFormSubmit() {
    if (!this.newPersonMode) {
      this.editPerson(this.personEditForm.value);
    } else {
      this.createPerson(this.personEditForm.value);
    }
    this.cancelPersonEdit();
  }  */

}
