import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { BehaviorSubject } from 'rxjs';
import { IPerson } from '../interfaces/IPerson';

@Injectable()
export class ApiService {

  private dataStore: {};

  private _allFromBackend: BehaviorSubject<any>;
  public allFromBackendToShow: Observable<any[]>;

  constructor(
    private http: HttpClient
  ) {
    this.dataStore = {};
    this._allFromBackend = <BehaviorSubject<any[]>>new BehaviorSubject([]);
    this.allFromBackendToShow = this._allFromBackend.asObservable();
  }

  public getAllItems(urlFragment: string) {
    return this.http.get('backend/' + urlFragment);
  }

  public getItem(urlFragment: string, id: number) {
      return this.http.get('backend/' + urlFragment + '/' + id);
  }

  public createItem(urlFragment, item: any) {
    return this.http.post('backend/' + urlFragment, item);
  }

  public updateItem(urlFragment, id: number, item: any) {
      return this.http.patch('backend/' + urlFragment + '/' + id, item);
  }

  public deleteItem(urlFragment: string, id: number) {
      return this.http.delete('backend/' + urlFragment + '/' + id);
  }
}



