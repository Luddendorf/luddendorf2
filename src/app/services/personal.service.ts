import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { BehaviorSubject } from 'rxjs';
import { Subscription } from 'rxjs';
import { IPerson } from '../interfaces/IPerson';

import { ApiService } from './api.service';

@Injectable()
export class PersonalService {

  public personalSub: Subscription;

  personalStore: any[] = [];

  selectedPerson: IPerson;

  urlFragment = 'person';

  state: any = {};

  public _personal: BehaviorSubject<IPerson[]>;
  public personal: Observable<IPerson[]>;

  constructor(
    private apiService: ApiService
  ) {
    this._personal = <BehaviorSubject<IPerson[]>>new BehaviorSubject([]);
    this.personal = this._personal.asObservable();
  }

  public getAndStoreAllPersonal() {
    this.apiService.getAllItems(this.urlFragment)
       .subscribe((res: any) => {
         this.personalStore = res.data.response.map((el) => ({
             id: el.id,
             name: el.name,
             role: el.role
         }));
         this._personal.next(this.personalStore);
        });
  }

  public getPerson(urlFragment: string, id: number) {
     this.apiService.getItem(urlFragment, id)
       .subscribe((res: any) => {
          this.selectedPerson = res.data.response;
       });
  }

  public createPerson(urlFragment: string, item: any): void {
    this.urlFragment = urlFragment;
    this.apiService.createItem(this.urlFragment, item)
       .subscribe((res: any) => {
         const {
            response: {
           id, name, role
         } } = res.data;
         const tempPers: IPerson = {
           id, name, role
         };
    this.personalStore.push(tempPers);
    this._personal.next([...this.personalStore]);
    });
  }

  public updatePerson(urlFragment: string, item: any, returnRequest: boolean = false): void {
     this.urlFragment = urlFragment;
     this.apiService.updateItem(this.urlFragment, item.id, item)
        .subscribe((res: any) => {
          const pos = this.personalStore.findIndex(i => i.id === item.id);
          if (pos !== -1) {
            this.personalStore.splice(pos, 1);
            this.personalStore.push(item);
          } else { throw Error('No such person.'); }
         this._personal.next([...this.personalStore]);
       });
    }

  public deletePerson(urlFragment: string, id: number): void {
     this.urlFragment = urlFragment;
     this.apiService.deleteItem(this.urlFragment, id)
        .subscribe(res => {
          this.personalStore = this.personalStore.filter((el) => el.id !== id);
          this._personal.next(this.personalStore);
         });
  }
}

