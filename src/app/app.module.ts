import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { MatButtonModule } from '@angular/material';
import { MatListModule } from '@angular/material/list';
import { MatIconModule } from '@angular/material/icon';
import { MatDialogModule } from '@angular/material/dialog';
import { MatSelectModule } from '@angular/material/select';
import { MatInputModule } from '@angular/material';
import { HttpClientModule } from '@angular/common/http';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { ApiService } from './services/api.service';
import { PersonalService } from './services/personal.service';
import { PersonalComponent } from './personal/personal.component';
import { RolesComponent } from './roles/roles.component';
import { PersonEditFormComponent } from './personal/person-edit-form/person-edit-form.component';


@NgModule({
  declarations: [
    AppComponent,
    PersonalComponent,
    RolesComponent,
    PersonEditFormComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MatButtonModule,
    MatListModule,
    MatIconModule,
    MatDialogModule,
    MatSelectModule,
    MatInputModule,
    FormsModule,
    ReactiveFormsModule,
  ],
  providers: [ApiService, PersonalService],
  bootstrap: [AppComponent],
  entryComponents: [PersonEditFormComponent]
})
export class AppModule {  }
